#include <vector>

// ROOT include(s):
#include <TChain.h>
#include <TString.h>
#include <TCanvas.h>
#include <TError.h>
#include <TH1D.h>
#include <TNtupleD.h>
#include <TFile.h>

// xAOD include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODRootAccess/tools/Message.h"

// EDM include(s):
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"

using namespace std;

void RenormalizeHistByBinWidth(TH1D* hist, bool undo);
TCanvas* plotter(TNtupleD* tuple, TString var, TString cond = "", double min=999, double max=-999);

int main(int argc, char* argv[])
//int main()
{

   // The application's name:
   static const char* APP_NAME = "MC15Validation";

   // Initialise the environment for xAOD reading:
   RETURN_CHECK( APP_NAME, xAOD::Init( APP_NAME ) );

   TString file = argc == 2 ? string(argv[1]) : "DAOD_TRUTH0.TRUTH.root";

   // Create a TChain with the input file(s):
   TChain chain( "CollectionTree" );
   chain.Add( TString(file) );

   // Create a TEvent object with this TChain as input:
   //xAOD::TEvent event;
   xAOD::TEvent event( xAOD::TEvent::kBranchAccess );
   RETURN_CHECK( APP_NAME, event.readFrom( &chain ) );

   vector<TString> eventVec = {"n_l1","pdgid_l1","e_l1","px_l1","py_l1","pz_l1","pt_l1","eta_l1","phi_l1","n_l2","pdgid_l2","e_l2","px_l2","py_l2","pz_l2","pt_l2","eta_l2","phi_l2","m_ll"};
   TString eventIndex = "";
   for(int i=0; i<eventVec.size(); i++)
   {
       if(i>0) eventIndex += ":";
       eventIndex += eventVec[i];
   }
   TNtupleD* events = new TNtupleD("events","events",eventIndex);

   Double_t GeV = 0.001;


      Double_t n_l1=0, pdgid_l1=0, e_l1=0, px_l1=0, py_l1=0, pz_l1=0, pt_l1=0, eta_l1=0, phi_l1=0;
      Double_t n_l2=0, pdgid_l2=0, e_l2=0, px_l2=0, py_l2=0, pz_l2=0, pt_l2=0, eta_l2=0, phi_l2=0;
      Double_t m_ll=0;

   // Loop over the input file(s):
   const ::Long64_t entries = event.getEntries();
   for( ::Long64_t entry = 0; entry < entries; ++entry ) {

      // Load the event:
      if( event.getEntry( entry ) < 0 ) {
         Error( APP_NAME, XAOD_MESSAGE( "Failed to load entry %i" ),
                static_cast< int >( entry ) );
         return 1;
      }

      // Load the truth particles from it:
      const xAOD::TruthParticleContainer* mc_particle = 0;
      RETURN_CHECK( APP_NAME, event.retrieve( mc_particle, "TruthParticles" ) );
      if (entry%1000 == 0) Info( APP_NAME, "Event Number: %i", static_cast< int >( entry ));

      n_l1=0; pdgid_l1=-999; e_l1=-999; px_l1=-999; py_l1=-999; pz_l1=-999; pt_l1=-999; eta_l1=-999; phi_l1=-999;
      n_l2=0; pdgid_l2=-999; e_l2=-999; px_l2=-999; py_l2=-999; pz_l2=-999; pt_l2=-999; eta_l2=-999; phi_l2=-999;
      m_ll=-999;

//      std::cout << "EVENT " << entry << std::endl;

      for(auto truth = mc_particle->begin(); truth!=mc_particle->end(); ++truth) {
	
	Int_t PDG_ID = (*truth)->pdgId();
	Int_t Status = (*truth)->status();
        bool isMuon = PDG_ID==13 && Status==23;
        bool isAntiMuon = PDG_ID==-13 && Status==23;
        bool isElectron = PDG_ID==11 && Status==23;
        bool isAntiElectron = PDG_ID==-11 && Status==23;

        if(isMuon or isElectron)
        {
            n_l1++;
            pdgid_l1=PDG_ID;
            e_l1=(*truth)->e()*GeV;
            px_l1=(*truth)->px()*GeV;
            py_l1=(*truth)->py()*GeV;
            pz_l1=(*truth)->pz()*GeV,
            pt_l1=(*truth)->pt()*GeV;
            eta_l1=(*truth)->eta();
            phi_l1=(*truth)->phi();
        }
        else if(isAntiMuon or isAntiElectron)
        {
            n_l2++;
            pdgid_l2=PDG_ID;
            e_l2=(*truth)->e()*GeV;
            px_l2=(*truth)->px()*GeV;
            py_l2=(*truth)->py()*GeV;
            pz_l2=(*truth)->pz()*GeV,
            pt_l2=(*truth)->pt()*GeV;
            eta_l2=(*truth)->eta();
            phi_l2=(*truth)->phi();
        }

      }

   m_ll=sqrt(fabs(2*pt_l1*pt_l2*(cosh(eta_l1-eta_l2)-cos(phi_l1-phi_l2))));

   vector<double> fill{n_l1,pdgid_l1,e_l1,px_l1,py_l1,pz_l1,pt_l1,eta_l1,phi_l1,n_l2,pdgid_l2,e_l2,px_l2,py_l2,pz_l2,pt_l2,eta_l2,phi_l2,m_ll};

   events->Fill(&fill[0]);

   }

   TFile *savefile = new TFile("events.root","RECREATE");
   events->Write();

   TCanvas* can_var = 0;

   for(int i=0; i<eventVec.size(); i++)
   {
       can_var = plotter(events,eventVec[i]);
       can_var->Write();
   }

   savefile->Close();

   // Return gracefully:
   return 0;
}

void RenormalizeHistByBinWidth(TH1D* hist, bool undo)
{
    vector<double> binEdges;
    vector<double> binCenters;
    vector<double> weights;
    vector<double> errors;

    for (int i=1; i <= hist->GetXaxis()->GetNbins(); i++)
    {
        binEdges.push_back(hist->GetXaxis()->GetBinLowEdge(i));
        binCenters.push_back(hist->GetXaxis()->GetBinCenter(i));
        if(undo)
        {
            weights.push_back( hist->GetBinContent(i)*hist->GetXaxis()->GetBinWidth(i) );
            errors.push_back(hist->GetBinError(i-1)*hist->GetXaxis()->GetBinWidth(i) );
        }
        else
        {
            weights.push_back( hist->GetBinContent(i) / hist->GetXaxis()->GetBinWidth(i) );
            errors.push_back(hist->GetBinError(i-1) / hist->GetXaxis()->GetBinWidth(i) );
        }
    }

    binEdges.push_back(hist->GetXaxis()->GetBinUpEdge(hist->GetXaxis()->GetNbins()));
    hist->Reset();
    hist->FillN(binCenters.size(),&binCenters[0],&weights[0]);
    hist->SetError(&errors[0]);
}

TCanvas* plotter(TNtupleD* tuple, TString var, TString cond, double min, double max)
{
    TCanvas* can = new TCanvas("can_"+var,"can_"+var,800,600);

    int n = tuple->Draw(var, cond, "goff");
    if(n==0) cout << "empty!" << endl;

    TH1D* hist1 = (TH1D*)tuple->GetHistogram();
    hist1->SetName(var);
    hist1->GetXaxis()->SetTitle(var);
    //RenormalizeHistByBinWidth(hist1,false);
    if(min<max) hist1->GetXaxis()->SetLimits(min,max);
    hist1->Draw("h");

    return can;
}
