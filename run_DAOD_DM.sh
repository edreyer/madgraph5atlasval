#Set up the validation step
cd source/MCVal
setupATLAS
asetup 20.7.3.4,AtlasDerivation,here

#JO loop
for channel in 'ee' 'mumu'; do
    for mass in '0p5' '1p0' '2p0'; do

        coupling='0p02'

        tag="${channel}_${mass}_${coupling}_DM"

        #Copy output EVNT.root to MCVal area
        cp "../../run_${tag}/EVNT.root" "EVNT_${tag}.root"

        #Create DAOD_TRUTH0.TRUTH.root file from EVNT.root
        Reco_tf.py --inputEVNTFile "EVNT_${tag}.root" --outputDAODFile TRUTH.root --reductionConf TRUTH0

        #Move DAOD_TRUTH0.TRUTH.root to where MCVal wants it
        mv DAOD_TRUTH0.TRUTH.root ../DAOD_TRUTH0_${tag}.TRUTH.root

    done
done
