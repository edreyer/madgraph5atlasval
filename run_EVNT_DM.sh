control="MadGraphControl_MGPy8EG_N30LO_A14N23LO_dmA_ll.py"
param='MadGraph_param_card_DMsimp_s_spin1.dat'

#Setup for EVNT generation
setupATLAS
asetup 19.2.5.34.2,MCProd,here

#JO loop
for channel in 'ee' 'mumu'; do
    for mass in '0p5' '1p0' '2p0'; do

        coupling='0p02'

        tag="${channel}_${mass}_${coupling}_DM"

        jobOptions="MC15.999999.MGPy8EG_N30LO_A14N23LO_DMsA_${channel}_mR${mass}_gSM${coupling}.py"

        #Initialize work area
        mkdir "run_${tag}"
        cd "run_${tag}"
        cp "../share/$control" "../share/$jobOptions" "../share/$param" .

        #Create EVNT file
        Generate_tf.py --ecmEnergy=13000. --maxEvents=10000 --runNumber=999999 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=EVNT.root --jobConfig=${jobOptions}

        cd ..
    done
done
