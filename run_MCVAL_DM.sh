#Set up the validation step
cd source/MCVal
source Setup.sh
#make clean
#make

mkdir events_DM

#JO loop
for channel in 'ee' 'mumu'; do
    for mass in '0p5' '1p0' '2p0'; do

        coupling='0p02'

        tag="${channel}_${mass}_${coupling}_DM"

        #Run validation
        ./MC15Validation ../DAOD_TRUTH0_${tag}.TRUTH.root
        mv events.root events_DM/events_${tag}.root

    done
done
